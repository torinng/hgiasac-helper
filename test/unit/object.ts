import * as test from "tape";
import { deepCamelCaseObject } from "../../src";
// tslint:disable
test('Test positive toCamelCaseObject', (t) => {
  const obj = {
    parent_property: {
      child_property: 1,
      children: [1, 2],
      children_properties: [{ grandchild_property: 'test' }]
    },
    friend: 'john',
  };

  const result = deepCamelCaseObject(obj);
  t.equal(typeof result.parentProperty, 'object');
  t.equal(result.parentProperty.childProperty, 1);
  t.equal(result.parentProperty.children[0], 1);
  t.equal(result.parentProperty.children[1], 2);
  t.equal(Array.isArray(result.parentProperty.childrenProperties), true);
  t.equal(result.parentProperty.childrenProperties[0].grandchildProperty, 'test');

  t.end();
});
