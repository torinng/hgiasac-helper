import * as test from "tape";
import { getUri, parseQueryString } from "../../src";
// tslint:disable
test('Test uri and param encoding', (t) => {
  const baseUri = 'http://localhost:8000';
  const params = {
    parent: {
      child: 1,
      children: [1, 2],
    },
    friend: 'john',
  };

  const result = getUri(baseUri, params);
  t.equal(result, baseUri + '?parent[child]=1&parent[children]=1,2&friend=john', 'URL is equal');

  t.end();
});

test('test parse uri then encoding', (t) => {
  const query = 'parent[child]=1&parent[children][grandchild]=1,2&friend=john';

  const params = parseQueryString(query);
  console.error(params);
  t.equal(params.parent.child, '1', 'Parent child must equal 1');
  t.equal(params.parent.children.grandchild[0], '1', 'Parent childrent must equal array');
  t.equal(params.parent.children.grandchild[1], '2', 'Parent childrent must equal array');
  t.equal(params.friend, 'john', 'Friend must equal john');

  const result = getUri('/', params);
  t.equal(result, '/?' + query, 'URL is equal');

  t.end();
});
