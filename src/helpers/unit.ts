
const LENGTH_UNITS = ["mm", "cm", "dm", "m", "dam", "ha", "km"];
const TIME_UNITS = [
  { key: "s", value: 60 },
  { key: "m", value: 60 } ,
  { key: "h", value: 24 },
  { key: "d", value: 30 },
  { key: "M", value: 12 },
];

/**
 * Converts length unit
 *
 * @param  {number} value Input value
 * @param  {string} fromUnit From length unit
 * @param  {string} toUnit To length unit
 *
 * @return {number}       Value in toUnit
 */
function convertUnitLength(value: number, fromUnit: string, toUnit: string): number {
  if (!value) {
    return 0;
  }

  if (fromUnit === toUnit) {
    return value;
  }

  const fromIndex = LENGTH_UNITS.indexOf(fromUnit);
  const toIndex = LENGTH_UNITS.indexOf(toUnit);
  const sub = fromIndex - toIndex;

  return value * Math.pow(10, sub);
}

  /**
   * Convert value in time unit
   *
   * @param  {number} value Input value
   * @param  {string} fromUnit From length unit
   * @param  {string} toUnit To length unit
   *
   * @return {number}       Value in kilometre
   */
function convertUnitTime(value: number, fromUnit: string, toUnit: string): number {
  if (!value) {
    return 0;
  }

  if (fromUnit === toUnit) {
    return value;
  }

  let i = TIME_UNITS.length;
  let fromIndex = -1;
  let toIndex = -1;
  let result = value;

  for (i = TIME_UNITS.length - 1; i >= 0; i--) {
    if (TIME_UNITS[i].key === fromUnit) {
      fromIndex = i;
    }
    if (TIME_UNITS[i].key === toUnit) {
      toIndex = i;
    }

    if (fromIndex > -1 && toIndex > -1) {
      break;
    }
  }

  if (fromIndex < toIndex) {
    for (i = fromIndex; i < toIndex; i++) {
      result /= TIME_UNITS[i].value;
    }
  } else {
    for (i = fromIndex; i > toIndex; i--) {
      result *= TIME_UNITS[i].value;
    }
  }

  return result;
}

/**
 * Converts metre to kilometre
 *
 * @param  {number} value Input value
 *
 * @return {number}       Value in kilometre
 */
function metreToKilometre(value: number): number {
  if (!value) {
    return 0;
  }

  return value / 1000;
}

/**
 * Converts kilometre to metre
 *
 * @param  {number} value Input value
 *
 * @return {number}       Value in metre
 */
function kilometreToMetre(value: number): number {
  if (!value) {
    return 0;
  }

  return value * 1000;
}

export {
  convertUnitLength,
  convertUnitTime,
  metreToKilometre,
  kilometreToMetre,
};
