import { camelCase, isEmpty } from "lodash";
export {
  getFirstElement,
  getFirstKey,
  objectToArray,
};

export interface IAssignOption {
  override?: boolean;
  keepDest?: boolean;
  excepts?: string[] | string;
  attributes?: string[] | string;
  stripNull?: boolean;
  prefix?: string;
}

/**
 * Get First element of object
 *
 * @param  {Object} obj Object data
 * @return {any}        Element data
 */
function getFirstElement(obj: object): any {
  if (isEmpty(obj)) {
    return null;
  }

  return obj[Object.keys(obj)[0]];
}

/**
 * Get first key of object
 *
 * @param  {Object} obj Object data
 * @return {string}     Object key
 */
function getFirstKey(obj: object): string {
  if (isEmpty(obj)) {
    return null;
  }

  return Object.keys(obj)[0];
}

export function stripObject(input: object, emptyValues: any[] = [null, undefined, ""]) {
  return Object.keys(input).reduce((o, k) => emptyValues.indexOf(input[k]) === -1
    ? { ...o, [k]: input[k] } : o, {});
}

export function diff<T = object>(obj1: object, obj2: object): T {

  return <T> Object.keys(obj1).reduce((r, k) => obj2 === undefined
    || obj1[k] === obj2[k] ? r : { [k]: obj2[k], ...r }, {});
}

/**
 * Convert object to array
 *
 * @param  {Object}        input Object data
 * @param  {IAssignOption} opts  Option data
 *
 * @return {Array<any>}        Array data
 */
function objectToArray(input: object, opts?: IAssignOption): any[] {

  const options = {
    excepts: [],
    keyName: "key",
    valueName: "value", ...opts};

  return Object.keys(input).filter((key) => opts.excepts.indexOf(key) !== -1)
    .map((key) => ({ [options.keyName]: key, [options.valueName]: input[key] }));
}

export function deepCamelCaseObject(src: object): {[key: string]: any} {

  return Object.keys(src).reduce((r, k) => {
    switch (typeof src[k]) {
    case "object":
      if (Array.isArray(src[k])) {
        r[camelCase(k)] = src[k].length > 0 && typeof src[k][0] === "object"
            ? src[k].map(deepCamelCaseObject) : src[k];

      } else {
        r[camelCase(k)] = deepCamelCaseObject(src[k]);
      }
      break;
    default:
      r[camelCase(k)] = src[k];
    }

    return r;
  }, {});
}
