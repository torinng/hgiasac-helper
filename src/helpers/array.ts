
/**
 * Filter array with limit
 *
 * @type {Array<T>}
 */
export function filterLimit<T>(src: T[], fn: (T) => boolean, limit: number = 1): T[] {

  if (!src || src.length === 0) {
    return [];
  }

  const results = [];
  let count = 0;
  src.every((item) => {
    if (fn(item)) {
      results.push(item);
      count++;
      if (count === limit) {
        return false;
      }
    }

    return true;
  });

  return results;
}

/**
 * From distinct array
 *
 * @type {Array<T2>}
 */
export function fromDistinct<T1, T2>(
  src: T1[],
  fn: (x: T1) => T2,
  distinctFn?: (xs: T1[], x: T1) => boolean): T2[] {

  if (!src || src.length === 0) {
    return [];
  }

  if (!distinctFn) {
    return src.reduce((acc, m) => {
      const val = fn(m);

      return acc.indexOf(val) !== -1 ? acc : acc.concat([val]);
    }, []);
  }

  const results = [];
  for (let i = 0; i < src.length; ++i) {
    const m = src[i];

    if (!distinctFn(src.slice(0, i + 1), m)) {
      continue;
    }

    results.push(fn(m));
  }

  return results;
}

export function splitArray<T = any>(input: T[], condition: (x: T) => boolean): T[][] {

  const tList = [];
  const fList = [];

  if (!input || input.length === 0) {
    return [tList, fList];
  }

  for (const item of input) {
    if (condition(item)) {
      tList.push(item);
    } else {
      fList.push(item);
    }
  }

  return [tList, fList];
}
