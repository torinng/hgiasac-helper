import { clone } from "lodash";

/**
 * Checks input value is numeric
 *
 * @param  {any}  n   Input number
 *
 * @return {boolean}
 */
export function isNumeric(n: any): boolean {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

export function merge(target: object | any[], source: object | any[],
                      isNullOverride: boolean = true, isMergeArrays: boolean = true) {

  if (!source) {
    return target;
  }

  if (Array.isArray(source)) {
    if (!Array.isArray(target)) {
      throw new Error("Cannot merge array onto an object");
    }
    if (isMergeArrays === false) {       // isMergeArrays defaults to true
      (<any[]> target).length = 0; // Must not change target assignment
    }

    for (const src of source) {
      target.push(clone(src));
    }

    return target;
  }

  const keys = Object.keys(source);
  for (const key of keys) {
    const value = source[key];
    if (value && typeof value === "object") {

      if (!target[key] ||
        typeof target[key] !== "object" ||
        (Array.isArray(target[key]) !== Array.isArray(value)) ||
        value instanceof Date ||
        Buffer.isBuffer(value) ||
        value instanceof RegExp) {

        target[key] = exports.clone(value);
      } else {
        exports.merge(target[key], value, isNullOverride, isMergeArrays);
      }
    } else if (value !== null && value !== undefined) {

      target[key] = value;
    } else if (isNullOverride !== false) {                    // Defaults to true
      target[key] = value;
    }
  }

  return target;
}
