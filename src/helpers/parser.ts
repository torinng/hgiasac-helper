export function safeParseInt(input: number | string, radix = 10): number {

  return typeof input === "number" ? input : parseInt(input, radix);
}

export function safeParseFloat(input: number | string): number {

  return typeof input === "number" ? input : parseFloat(input);
}

export function safeParseJSON<T = object>(input: object | string, defaultValue = {}): T {

  try {
    return typeof input === "object" ? input : JSON.parse(input);
  } catch (e) {
    return <T> defaultValue;
  }
}
