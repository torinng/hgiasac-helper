/**
 * The product of all the whole numbers between 1 and n, where n must always be positive.
 * A factorial is represented by the sign (!)
 *
 * @param  {number} value Input value
 *
 * @return {number}       Factorial result
 */
function factorial(value: number): number {

  if (value < 0) {
    throw new Error(`Input value must be positive: ${value}`);
  }

  if (!value) {
    return 1;
  }

  let result = value;

  for (let i = value - 1; i > 1; i--) {
    result = result * i;
  }

  return result;
}

/**
 * Permutations of n items taking r items at a time
 * Is represented by the sign (nPr)
 *
 * @param  {number} n Total items
 * @param  {number} r Number of items to be taken of
 *
 * @return {number}   Number of ways to select
 */
function permutations(n: number, r: number): number {
  if (n < r) {
    throw new Error(`n must larger than or equal to r. n: ${n}, r: ${r}`);
  }

  return factorial(n) / factorial(n - r);
}

/**
 * Combinations of n items taking r items at a time
 * Is represented by the sign (nCr)
 *
 * @param  {number} n Total items
 * @param  {number} r Number of items to be taken of
 *
 * @return {number}   Number of ways to select
 */
function combinations(n: number, r: number): number {
  if (n < r) {
    throw new Error(`n must larger than or equal to r. n: ${n}, r: ${r}`);
  }

  return factorial(n) / (factorial(n - r) * factorial(r));
}

export {
  factorial,
  permutations,
  combinations,
};
