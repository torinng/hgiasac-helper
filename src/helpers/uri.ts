import { isEmpty } from "lodash";

interface IQueryObject {
  keys: string[];
  value: string;
}

/**
 * Apply subquery into query list
 * @param {Array<IQueryObject>} results     [description]
 * @param {IQueryObject}        queryObject [description]
 * @param {any}                 subQuery    [description]
 */
function applySubQuery(results: IQueryObject[], queryObject: IQueryObject, subQuery: any): void {

  if (Array.isArray(subQuery)) {
    queryObject.value = subQuery.join(",");

    return;
  }

  const valueType = typeof subQuery;
  if (valueType !== "object") {
    queryObject.value = valueType === "string" ? subQuery : subQuery.toString();

    return;
  }

  Object.keys(subQuery).forEach((key) => {
    const newQuery = {
      keys: [...queryObject.keys],
      value: "",
    };
    newQuery.keys.push(key);
    results.push(newQuery);
    applySubQuery(results, newQuery, subQuery[key]);
  });

  const index = results.indexOf(queryObject);
  if (index !== -1) {
    results.splice(index, 1);
  }
}

/**
 * Generate query from params
 *
 * @param  {object} params query params
 *
 * @return {string}        query url string
 */
function generateQuery(params: object): string {

  if (isEmpty(params)) {
    return "";
  }

  const results = [];
  const queryParams = [];
  Object.keys(params).forEach((key) => {
    const item = {
      keys: [key],
      value: "",
    };

    results.push(item);
    applySubQuery(results, item, params[key]);
  });

  results.forEach((element) => {
    let textParam = "";
    for (let i = 0; i < element.keys.length; i++) {
      textParam += i === 0 ? element.keys[i] : `[${element.keys[i]}]`;
    }

    textParam += "=" + element.value;

    queryParams.push(textParam);
  });

  return queryParams.length === 0 ? "" : queryParams.join("&");
}

export interface IHostOption {
  host: string;
  port: string;
  basePath: string;
}

/**
 * Get endpoint uri from options
 *
 * @param  {IHostOption} options endpoint options
 * @param  {string} query   query string
 *
 * @return {string}         endpoint URI
 */
function getEndpoint(options: IHostOption, query: string) {

  let endpoint = options.host;

  if (options.port) {
    endpoint += ":" + options.port;
  }

  if (options.basePath) {
    endpoint += options.basePath;
  }

  return endpoint + query;
}

export interface IParseQueryOption {
  allowEmpty: boolean;
}

/**
 * Apply parse query option
 *
 * @param {IParseQueryOption} options Query option
 */
function applyParseQueryOption(options?: IParseQueryOption) {
  return { allowEmpty: false, ...(options || {})};
}

/**
 * Parse query string to params
 *
 * @param {string} query Query string
 */
export function parseQueryString(query: string, options?: IParseQueryOption): { [key: string]: any } {

  const opts = applyParseQueryOption(options);

  let params = query.split("&");
  if (!opts.allowEmpty) {
    params = params.filter((q) => q);
  }

  const paramObj = params.reduce((r, q) => {
    const param = q.split("=");
    if (param[1]) {
      r[param[0]] = param[1];
    }

    return r;
  }, {});

  return parseQuery(paramObj, options);
}

/**
 * Parse query string to object
 *
 * @param  {string} params Parameter object
 *
 * @return {Object}        Query object
 */
function parseQuery(params: object, options?: IParseQueryOption): { [key: string]: any } {

  const opts = applyParseQueryOption(options);

  const result = {};
  const headPattern = /^([a-zA-Z]+)/;
  const childPattern = /\[([a-zA-Z]+)\]/g;

  Object.keys(params).forEach((key) => {

    const value = typeof params[key] !== "string" || params[key].indexOf(",") === -1
      ? params[key] : params[key].split(",");

    const childMatches = childPattern.exec(key);
    if (!childMatches) {
      result[key] = value;

      return;
    }

    const headMatches = headPattern.exec(key);
    if (!result[headMatches[1]]) {
      result[headMatches[1]] = {};
    }

    let iter = result[headMatches[1]];
    let subkey = childMatches[1];
    let match = childMatches;

    do {
      const m = childPattern.exec(key);
      if (!m) {
        iter[match[1]] = value;
        subkey = null;
        break;
      }
      iter[match[1]] = iter[match[1]] || {};
      iter = iter[match[1]];
      match = m;
      subkey = match[1];
    } while (match);

    if (subkey) {
      iter[subkey] = value;
    }
  });

  if (!opts.allowEmpty) {
    removeEmptyProperties(result);
  }

  return result;
}

export function removeEmptyProperties(obj: object): boolean {
  let count = 0;
  Object.keys(obj).forEach((key) => {
    if (!obj[key]) {
      delete obj[key];
    }

    if (!Array.isArray(obj[key]) && typeof obj[key] === "object") {
      if (removeEmptyProperties(obj[key])) {
        count++;
      } else {
        delete obj[key];
      }
    }

    count++;
  });

  return count > 0;
}

export {
  generateQuery,
  getEndpoint,
  parseQuery,
};

/**
 * Generates URI from path with params
 *
 * @param {string} uri      URI string
 * @param {Object} params   Query params
 *
 * @return {string}          Absolute URI
 */
export function getUri(uri: string, params: object): string {

  if (!isEmpty(params)) {
    const queryParams = generateQuery(params);

    if (queryParams) {
      return `${uri}?${queryParams}`;
    }
  }

  return uri;
}
