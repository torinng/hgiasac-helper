import * as crypto from "crypto";
import { padStart, replace } from "lodash";
/* tslint:disable */
const phoneCodes = require('../../resources/phone-codes.json');
/* tslint:enable */
export interface IReplaceParams {
  [key: string]: any;
}

/**
 * Apply phone code into phone number
 * @param  {string} phoneNumber Phone number
 * @param  {string} phoneCode   Phone code
 * @return {string}             Phone number with phone code
 */
function internationalPhoneNumber(phoneNumber: string, countryCode: string): string {

  const phoneCode = phoneCodes[countryCode];

  return applyPhoneCode(phoneNumber, phoneCode);
}

/**
 * Apply phone code into phone number
 * @param  {string} phoneNumber Phone number
 * @param  {string} phoneCode   Phone code
 * @return {string}             Phone number with phone code
 */
export function applyPhoneCode(phoneNumber: string, phoneCode: string): string {
  if (phoneNumber.substr(0, phoneCode.length) === phoneCode) {
    return phoneNumber;
  }

  if (phoneNumber.substr(0, 1) === "0") {
    return phoneCode + phoneNumber.substr(1);
  }

  return phoneCode + phoneNumber;
}

/**
 * Apply phone code into phone number
 * @param  {string} phoneNumber Phone number
 * @param  {string} phoneCode   Phone code
 * @return {string}             Phone number with phone code
 */
export function phoneNumberE164(phoneNumber: string, phoneCode: string): string {
  return "+" + applyPhoneCode(phoneNumber, phoneCode);
}

/**
 * Remove phone code from international phone number
 *
 * @param  {string} phoneNumber Input phone number
 * @param  {string} countryCode Country code
 *
 * @return {string}             locale phone number
 */
function removePhoneCode(phoneNumber: string, phoneCode: string): string {

  if (phoneNumber.substr(0, 1) === "0") {
    return phoneNumber;
  }

  const codeLength = phoneCode.length;

  return "0" + phoneNumber.substr(codeLength);
}

/**
 * Remove phone code from phone number
 * @param  {string} phoneNumber International phone number
 * @param  {string} countryCode Phone code
 * @return {string}             Localized phone number
 */
function localePhoneNumber(phoneNumber: string, countryCode: string): string {

  const phoneCode = phoneCodes[countryCode];

  return removePhoneCode(phoneNumber, phoneCode);

}

/**
 * Check if string is empty
 *
 * @param  {string}  input Input string
 *
 * @return {boolean}
 */
function isEmpty(input: string): boolean {
  if (!input) {
    return true;
  }

  if (typeof input !== "string") {
    throw new Error("Input data must be a string");
  }

  return input.trim() === "";
}

/**
 * Creates random string with base64 format
 *
 * @param  {number} len the length of result string
 *
 * @return {string}     random string
 */
function randomBase64(len: number): string {
  return crypto.randomBytes(Math.ceil(len * 3 / 4))
    .toString("base64") // convert to base64 format
    .slice(0, len) // return required number of characters
    .replace(/\+/g, "0") // replace '+' with '0'
    .replace(/\//g, "0"); // replace '/' with '0'
}

/**
 * Generate random string with limited character scope
 *
 * @param  {number} len   String length
 * @param  {string} chars Character scope
 *
 * @return {string}       Outputed random chars
 */
function randomChars(len: number, chars?: string): string {
  chars = chars || "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
  const rnd = crypto.randomBytes(len);
  const value = new Array(len);
  const charLength = chars.length;

  for (let i = 0; i < len; i++) {
    value[i] = chars[rnd[i] % charLength];
  }

  return value.join("");
}

/**
 * Generate random number string
 *
 * @param  {Integer} len   String length
 * @param  {String} chars Character scope
 *
 * @return {String}       Outputed random number characters
 */
function randomNumberChars(len: number): string {
  return randomChars(len, "0123456789");
}

/**
 * Generates random token
 *
 * @return {string} Random token string
 */
function generateRandomToken(): string {

  const buffer = crypto.randomBytes(256);

  return crypto.createHash("sha1")
    .update(buffer)
    .digest("hex");
}

/**
 * Checks if input string is phone number
 *
 * @param  {string}  input Input string
 * @return {boolean}       Is valid phone number
 */
function isPhoneNumber(input: string): boolean {
  return /^[0-9]+$/.test(input);
}

/**
 * Check if input string is email
 *
 * @param  {string}  input Input string
 *
 * @return {boolean}       Is valid email
 */
function isEmail(input: string): boolean {
  const emailPattern = /[A-Z0-9\._%\+-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}/igm;

  return emailPattern.test(input);
}

/**
 * Replaces list dictionary key => value with pattern
 *
 * @param  {string} input       Input text
 * @param  {Object} params      Key value params
 *
 * @return {string}             Result output string
 */
function replaceParams(input: string, params: IReplaceParams): string {

  return Object.keys(params).reduce((result, k) => {
    const val = params[k];

    return replace(result, `{{${k}}}`, val);
  }, input);
}

/**
 * Format number with mark character
 *
 * @param  {number} input Number needs to be formated
 * @param  {string} mark  Decimal mark character
 *
 * @return {string}       Formated input
 */
function formatNumber(input: number, mark = ","): string {

  if (input < 1000) {
    return input.toString();
  }

  let result = "";
  while (input) {

    const mod = input % 1000;
    input = Math.floor(input / 1000);

    const str = input === 0 ? mod.toString() : padStart(mod.toString(), 3, "0");
    result = result ? str + mark + result : str;
  }

  return result;
}

export function camelCaseToPascal(input: string): string {
  if (!input || !input.length) {
    return "";
  }

  return input.substring(0, 1).toUpperCase() + input.substring(1);
}

export {
  internationalPhoneNumber,
  localePhoneNumber,
  isEmpty,
  randomBase64,
  randomChars,
  randomNumberChars,
  generateRandomToken,
  isPhoneNumber,
  isEmail,
  replaceParams,
  formatNumber,
};
