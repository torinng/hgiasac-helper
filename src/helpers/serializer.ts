export interface I18nOptions {
  language?: string;
  timezone?: string;
}

export interface ISerializationOptions extends I18nOptions {
  imageSize?: string;
}
