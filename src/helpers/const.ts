export const STATUS = {
  ACTIVE: "active",
  DELETED: "deleted",
  DISABLED: "disabled",
  INACTIVE: "inactive",
};

export const IsDebug = !process.env.NODE_ENV || process.env.NODE_ENV === "development"
  || process.env.IS_DEBUG === "true";
