import { safeParseJSON } from "../helpers";

export enum ImageSize {
  Original = "original"
}

export interface IFileInfo {

  bucketName?: string;
  filePath?: string;
  url: string;
  extension?: string;
}

export interface IFileDictionary {
  [key: string]: IFileInfo;
}

export function getImageUrl(
    dict: string | IFileDictionary, key: string, defaultKey = ImageSize.Original): string {

  const files = safeParseJSON(dict);

  return files[key] ? files[key].url : files[defaultKey] ? files[defaultKey].url : "";
}
