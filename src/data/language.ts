import { getFirstElement, safeParseJSON } from "../helpers";

export interface ILanguageDictionary {
  [key: string]: string;
}

export function getLocaleString(dictionary: string | ILanguageDictionary, locale: string): string {

  const dict = safeParseJSON(dictionary);

  return dict[locale] ? dict[locale] : getFirstElement(dict);
}
